package homework.day3;

import java.math.BigDecimal;
import java.util.Scanner;

public class TestDay3 {
    public static void main(String[] param) {
        MyDate a = new MyDate();
        MyDate b = new MyDate();

        System.out.println(a.MyDate);
        System.out.println(b.MyDate);

        b.MyDate = b.MyDate + 1;
        System.out.println(a.MyDate);

        Human a1 = new Student();
        Human a2 = new Worker();

        doSleep(a2);
        doSleep(a1);

        Scanner s = new Scanner(System.in);
        String nextToken = s.next(";");
        parse(nextToken);

        CanAccepMoney[] arr = new CanAccepMoney[3];
        arr[0] = new Student();
        arr[1] = new Worker();

        payMoney(100, arr);
    }

    static void payMoney(int money, CanAccepMoney[] acceptor) {
        for (int i = 0; i < acceptor.length; ++i) {
            acceptor[i].accept(money);
        }
    }

//    void payMoney(Student[] students) {
//        for (..) {
//
//        }
//    }
//
//    void payMoney(Student[] students) {
//        for (..) {
//
//        }
//    }

    static Object parse(String param) {
        try {
            Integer a = Integer.valueOf(param);
            return a;
        } catch (Exception ex) {
            try {
                BigDecimal bd = new BigDecimal(param);
                return bd;
            } catch (Exception ex2) {

            }
        }
        return null;
    }

    static void doSleep(Human human) {
        human.sleep();
    }
}
