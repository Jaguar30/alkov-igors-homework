package homework.day3;

public interface Warrior extends CanAccepMoney {
    void shoot();
}
