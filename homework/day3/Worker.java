package homework.day3;

public class Worker implements Human, Warrior {
    @Override
    public void see() {

    }

    @Override
    public void eat(Food food) {

    }

    @Override
    public void talk() {

    }

    @Override
    public void sleep() {
        System.out.println("Thank you boss!");
    }

    @Override
    public void shoot() {
        System.out.println("Shoot");
    }
}
